<?php

$session_cookie_name = 'SESSIONID';
$not_admin_cookie_name = 'allowed';
$allowed = 'true';

#strtolower was added after the competition to account for post requests straight to this webpage
if (strtolower(base64_decode($_COOKIE[$not_admin_cookie_name])) != $allowed){
        header('Location: index.php');
}
else{
        header('Location: loginform.html');
}

?>
