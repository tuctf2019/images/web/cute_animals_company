# Cute Animals Company -- Web -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/web/cute_animals_company)

## Chal Info

Desc: `Look at this cute website! Why don't you find some cute animals that are hidden from view?`

Flag: `TUCTF{m0r3_cut3_4n1m415_c4n_b3_f0und_4t_https://bit.ly/1HU2m5Q}`

## Dependencies

This relies on a MySQL database. Currently the creds are hardcoded, so there needs to be a database called `challenge` with the root password as `funnewpasswordtimes`. In the database, there should be a `users` table with an entry for a user called `bro` with password `ultimate699`:

```sql
CREATE TABLE users (user VARCHAR(20) NOT NULL, password VARCHAR(50) NOT NULL);
INSERT INTO users VALUES ('bro', 'ultimate699');
```

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/cute_animals_company)

Ports: 80

Environment Variables:

* MYSQL_ADDRESS: The address of the MYSQL server

Example usage:

```bash
docker run -d -p 127.0.0.1:8080:80 -e MYSQL_ADDRESS="127.0.0.1" asciioverflow/cute_animals_company:tuctf2019
```
